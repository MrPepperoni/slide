#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "slide_widget.h"

#include <QFrame>
#include <QPushButton>
#include <QGridLayout>
#include <QGraphicsOpacityEffect>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    auto central = new QFrame;
    auto cl = new QGridLayout{central};
    auto fr = new QFrame;
    fr->setMinimumSize(100,100);
    cl->addWidget(fr, 0 , 0);
    int row = 0;
    auto sl = new slide_widget;
    auto pb = new QPushButton{"left"};
    connect(pb, &QPushButton::clicked, [=]{sl->animate(QPoint{-1,0}, QPoint{0,0});});
    cl->addWidget(pb, ++row, 1);
    pb = new QPushButton{"right"};
    connect(pb, &QPushButton::clicked, [=]{sl->animate(QPoint{1,0}, QPoint{0,0});});
    cl->addWidget(pb, ++row, 1);
    pb = new QPushButton{"top"};
    connect(pb, &QPushButton::clicked, [=]{sl->animate(QPoint{0,-1}, QPoint{0,0});});
    cl->addWidget(pb, ++row, 1);
    pb = new QPushButton{"bottom"};
    connect(pb, &QPushButton::clicked, [=]{sl->animate(QPoint{0,1}, QPoint{0,0});});
    cl->addWidget(pb, ++row, 1);
    auto pb2 = new QPushButton{"WOOTING ONE"};
    pb2->setStyleSheet("min-width: 200; min-height: 200; background-color: white;");
    auto opa = new QGraphicsOpacityEffect{pb2};
    opa->setOpacity(0.5);
    pb2->setGraphicsEffect(opa);
    connect(pb2, &QPushButton::clicked, [=]{ static int red = 0; sl->viewport()->findChild<QFrame*>()->setStyleSheet((red = 1 - red) ? "background-color: red;":"background-color: blue");});
    sl->set_widget(pb2);
    cl->addWidget(sl, 1, 0, -1, 1);
    setCentralWidget(central);
}

MainWindow::~MainWindow()
{
    delete ui;
}
