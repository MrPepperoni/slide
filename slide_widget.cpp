#include "slide_widget.h"

#include <QFrame>

slide_widget::slide_widget(QWidget *parent)
    : QAbstractScrollArea{parent}
    , inner_container_{new QFrame}
{
    connect(this, &slide_widget::widget_position_changed, this, &slide_widget::on_widget_position_changed);
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    auto fr = new QFrame;
    fr->setStyleSheet("background-color: green;");
    setViewport(fr);
    inner_container_->setParent(viewport());
    inner_container_->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
}

slide_widget::~slide_widget()
{

}

QSize slide_widget::sizeHint() const
{
    return size_hint_;
}

QSize slide_widget::minimumSizeHint() const
{
    return size_hint_;
}

void slide_widget::set_widget(QWidget *widget)
{
    widget_ = widget;
    contained_size_ = widget->sizeHint();
    size_hint_ = contained_size_;
    auto container_size = contained_size_ * 3;
    widget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    inner_container_->setMinimumSize(container_size);
    inner_container_->move(-contained_size_.width(), -contained_size_.height());
    widget->setParent(inner_container_);
    widget->move(contained_size_.width(),contained_size_.height());

    update();
    viewport()->update();
}

void slide_widget::animate(QPoint from, QPoint to)
{
    from += QPoint{1,1};
    to += QPoint{1,1};
    current_anim_dir_ = from;
    anim_.reset(new QPropertyAnimation);
    anim_->setTargetObject(this);
    anim_->setPropertyName("widget_position");
    anim_->setStartValue(QPoint{from.x() * contained_size_.width(), from.y() * contained_size_.height()});
    anim_->setEndValue(QPoint{to.x() * contained_size_.width(), to.y() * contained_size_.height()});
    anim_->setDuration(2000);
    anim_->start();
}

void slide_widget::on_widget_position_changed(QPoint pos)
{
    if (widget_)
    {
        widget_->move(pos.x(), pos.y());
    }
    update();
    viewport()->update();
}
