#ifndef SLIDE_WIDGET_H
#define SLIDE_WIDGET_H

#include <QAbstractScrollArea>
#include <QPropertyAnimation>
#include <QScopedPointer>

class slide_widget : public QAbstractScrollArea
{
    Q_OBJECT
    Q_PROPERTY(QPoint widget_position MEMBER widget_position_ NOTIFY widget_position_changed)
public:
    slide_widget(QWidget* parent = nullptr);
    virtual ~slide_widget();
    QSize sizeHint() const override;
    QSize minimumSizeHint() const override;
    void set_widget(QWidget* widget);
Q_SIGNALS:
    void widget_position_changed(QPoint);
public Q_SLOTS:
    void animate(QPoint from, QPoint to);
private Q_SLOTS:
    void on_widget_position_changed(QPoint pos);
private:
    QWidget* inner_container_;
    QWidget* widget_ = nullptr;
    QPoint widget_position_;
    QSize size_hint_;
    QSize contained_size_;
    QPoint current_anim_dir_;
    QScopedPointer<QPropertyAnimation> anim_;
};

#endif // SLIDE_WIDGET_H
